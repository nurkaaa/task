#include "ThreadPool.h"

#include <iostream>
#include <iomanip>
#include <Windows.h>
#include <chrono> 

#include <vector>
#include <map>

#include <boost/filesystem.hpp>
#include <boost/thread.hpp>


namespace bfs = boost::filesystem;

int count_words(const std::string& filename)
{
	int counter = 0;
	std::ifstream file(filename);
	std::string buffer;
	while (file >> buffer)
	{
		++counter;
	}
	
	return counter;
}

int main(int argc, const char* argv[])
{
	std::cin.tie(0);
	std::ios_base::sync_with_stdio(false);

	bfs::path path = argv[1];
	// If this path is exist and if this is dir
	if (bfs::exists(path) && bfs::is_directory(path))
	{
		// Number of threads. Default = 4
		int n = (argc == 3 ? atoi(argv[2]) : 4);
		ThreadPool pool(n);

		// Container to store all filenames and number of words inside them
		//std::map<bfs::path, std::future<int>> all_files_and_sums;
		std::vector<std::future<int>> futures;
		
		auto start = std::chrono::high_resolution_clock::now();

		// Iterate all files in dir
		for (auto& p : bfs::directory_iterator(path)) {
			// Takes only .txt files
			if (p.path().extension() == ".log") {
				// Future for taking value from here
				auto fut = pool.enqueue([p]() {
					// In this lambda function I count all words in file and return this value
					int result = count_words(p.path().string());
					return result;
				});
				// "filename = words in this .txt file"
				futures.emplace_back(std::move(fut));
			}
		}

		int result = 0;

		for (auto& f : futures)
		{
			result += f.get();
		}

		auto stop = std::chrono::high_resolution_clock::now();

		auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);

		std::cout << "Result: " << result << "\n";
 
		std::cout << duration.count() << '\n';
	}
	else
		std::perror("Dir is not exist");
}