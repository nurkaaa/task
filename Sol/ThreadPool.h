#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <boost/thread/condition_variable.hpp>
#include <boost/thread.hpp>

#include <future> // I don't how to work with boost future
#include <queue>
#include <vector>
#include <functional>


class ThreadPool
{
public:
	using Task = std::function<void()>; // Our task

	explicit ThreadPool(int num_threads)
	{
		start(num_threads);
	}

	~ThreadPool()
	{
		stop();
	}

	template<class T>
	auto enqueue(T task)->std::future<decltype(task())>
	{
		// packaged_task wraps any Callable target
		auto wrapper = std::make_shared<std::packaged_task<decltype(task()) ()>>(std::move(task));

		{
			boost::unique_lock<boost::mutex> lock{ mutex_p };
			tasks_p.emplace([=] {
				(*wrapper)();
			});
			event_p.notify_one();
		}

		return wrapper->get_future();
	}

	/*void enqueue(Task task)
	{
		{
			boost::unique_lock<boost::mutex> lock { mutex_p };
			tasks_p.emplace(std::move(task));
			event_p.notify_one();
		}
	}*/

private:
	std::vector<boost::thread> threads_p; // num of threads
	std::queue<Task>           tasks_p;   // Tasks to make
	boost::condition_variable  event_p; 
	boost::mutex               mutex_p;

	bool                       isStop = false;

	void start(int num_threads)
	{
		for (int i = 0; i < num_threads; ++i)
		{
			// Add to the end our thread
			threads_p.emplace_back([=] {
				while (true)
				{
					// Task to do
					Task task;

					{
						boost::unique_lock<boost::mutex> lock(mutex_p);

						event_p.wait(lock, [=] { return isStop || !tasks_p.empty(); });

						// If we make all tasks
						if (isStop && tasks_p.empty())
							break;

						// Take new task from queue
						task = std::move(tasks_p.front());
						tasks_p.pop();
					}

					// Execute our task
					task();
				}
			});
		}
	}

	void stop() noexcept
	{
		{
			boost::unique_lock<boost::mutex> lock(mutex_p);
			isStop = true;
			event_p.notify_all();
		}

		for (auto& thread : threads_p)
		{
			thread.join();
		}
	}
};

#endif